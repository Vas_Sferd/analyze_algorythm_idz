// main.rs
//
// Copyright 2020 Vas_Sferd <vassferd@gmail.com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// SPDX-License-Identifier: GPL-3.0-or-later


mod graph;
use crate::graph::graphs::{Edge, Graph, TransportNetwork};

fn main() {
    let mut graph = Graph::new_empty();

    // Инициализируем граф
    vec![
        Edge::new(1, 2, 20),
        Edge::new(1, 3, 10),
        Edge::new(2, 3, 30),
        Edge::new(2, 4, 10),
        Edge::new(3, 4, 20),
    ].into_iter()
        .for_each(|e| graph.add_edge(e));

    let transport_network = TransportNetwork::new(1, 4, &graph);
    let flow = transport_network.max_flow();

    print!("{:?}", flow);
}
