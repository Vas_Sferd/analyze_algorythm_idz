// graph.rs
//
// Copyright 2020 Vas_Sferd <vassferd@gmail.com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// SPDX-License-Identifier: GPL-3.0-or-later


pub mod graphs {
    // Std
    use std::result::Result;
    use std::collections::HashMap;
    use std::fmt;

    #[derive(Clone, Default, Eq, Hash, Debug, PartialEq)]
    pub struct Edge {
        pub first: i32,
        pub second: i32,
        pub weight: i32,
    }

    impl Edge {
        pub fn new(first: i32, second: i32, weight: i32) -> Self {
            Edge {
                first,
                second,
                weight,
            }
        }
    }

    impl fmt::Display for Edge {
        fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
            write!(f, "({}, {}, {})", self.first, self.second, self.weight)
        }
    }

	#[derive(Clone, Default, Debug)]
	pub struct EdgePath (Vec<Edge>);

	impl EdgePath {
        // Добавление в путь
		pub fn try_push(&mut self, added_edge: Edge) -> Result<(), ()> {
			let last_edge = self.0.last().clone();

            match last_edge {
                Some(Edge { second: last_vertex, .. })
                    // Проверка на смежность добавляемого ребра и отсутствие циклов
                    if last_vertex == &added_edge.first
                    && self.0.iter().clone()
                        .all(|e|
                                e.second != added_edge.first
                            &&  e.second != added_edge.first
                        ) => Err(()),
                _ => {
                    self.0.push(added_edge);
					Ok(())
				},
			}
		}
	}

    #[derive(Clone, Default, Debug)]
    pub struct Graph {
        pub vertices: Vec<i32>,
        pub edges: Vec<Edge>,
    }

    impl Graph {
        // Создание пустого графа
        pub fn new_empty() -> Self {
            Self {
                vertices: Vec::new(),
                edges: Vec::new(),
            }
        }

        // Добавление новой вершины
        pub fn add_vertices(&mut self, vertice: i32) {
            if self.vertices.contains(&vertice) {
                self.vertices.push(vertice)
            };
        }

        // Добавление нового ребра
        pub fn add_edge(&mut self, edge: Edge) {
            self.add_vertices(edge.first);
            self.add_vertices(edge.second);
            self.edges.push(edge);
        }
    }

    impl fmt::Display for Graph {
        fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
            let mut out_line = String::new();

            out_line.push_str("{\n");

            for edge in &self.edges {
                &out_line.push_str(edge.to_string().as_str());
                &out_line.push_str(",\n");
            };

            out_line.push_str("}\n");

            write!(f, "{}", out_line)
        }
    }

    #[derive(Debug)]
    pub struct Flow<'a>(HashMap<&'a Edge, i32>);

    impl<'a> Flow<'a> {
        // Создание пустого потока для графа
        pub fn from_graph(graph: &'a Graph) -> Self {
            Self(
                graph.edges.iter()
                    .map(|edge| (edge, 0))
                    .collect::<HashMap<_,_>>()
            )
        }

        // Максимизирует поток
        pub fn maximize(mut self, source: i32, target: i32) -> Self
        {
            loop {
                match self.find_path(EdgePath(Vec::new()), source, target) {
                    Some(path) => {
                        self.augment(path);
                    },
                    None => break self,
                }
            }
        }

        // Бутылочное горлышко
        pub fn bottleneck(&self, path: &EdgePath) -> i32 {
            path.0.iter()
                .map(|edge| {
                    if self.0.contains_key(edge) {
                        edge.weight - *self.0.get(edge).unwrap_or(&0)
                    } else {
                        *self.0.get(& Edge {
                            first: edge.second,
                            second: edge.first,
                            weight: edge.weight,
                        }).unwrap_or(&0)
                    }
                })
                .min()
                .unwrap_or(0)
        }

        fn augment(&mut self, p: EdgePath) {
            let b = self.bottleneck(&p);

            p.0.iter()
                .for_each(|edge| {
                    if let Some((_, cur_flow)) = self.0.iter_mut()
                        .filter(|(old_edge, _)| edge == **old_edge)
                        .last() {

                        *cur_flow += b;
                    }
                    else if let Some((_, cur_flow)) = self.0.iter_mut()
                        .filter(|(old_edge, _)|
                            edge.first == old_edge.second
                            && edge.second == old_edge.first
                            && edge.weight == old_edge.weight
                        )
                        .last() {

                        *cur_flow -= b;
                    }
                });
        }

        // Поиск пути
        fn find_path(&self, mut path: EdgePath, source: i32, target: i32) -> Option<EdgePath> {
            let mut black_list_of_vertex = HashMap::<i32, ()>::new();

            loop {
                match path.0.last() {
                    Some(e) if e.second == target => break Some(path),
                    opt_e => {
                        let current = match opt_e {
                            Some(e) => e.second,
                            None => source,
                        };

                        // Поиск следующего ребра
                        let next_edge = self.0.iter()
                            .filter(|(&e, &f)|
                                    e.first == current && e.weight > f
                                ||  e.second == current && f > 0
                            )
                            .map(|(&e, _)|
                                if e.second == current {
                                    Edge {
                                        first: e.second,
                                        second: e.first,
                                        weight: e.weight,
                                    }
                                } else {
                                    e.clone()
                                }
                            )
                            .filter(|e| !black_list_of_vertex.contains_key(&e.second))
                            .next();

                        // Наполнение пути
                        match next_edge {
                            // Идём вперед
                            Some(next_edge) => {
                                let last_vertex = next_edge.second;
                                black_list_of_vertex.insert(last_vertex, ());

                                path.try_push(next_edge).unwrap();
                            }
                            // Наткнулись на тупик
                            None => {
                                match path.0.pop() {
                                    Some(_) => continue,
                                    None => {
                                        // Путь невозможен!
                                        break None;
                                    },
                                }
                            },
                        }
                    },
                }
            }
        }
    }

	pub struct TransportNetwork<'a> {
		source: i32,
		target: i32,
		graph: &'a Graph,
	}

	impl<'a> TransportNetwork<'a> {
		pub fn new(source: i32, target: i32, graph: &'a Graph) -> Self {
            TransportNetwork {
                source,
                target,
                graph,
            }
        }

		pub fn max_flow(&self) -> Flow {
            Flow::from_graph(&self.graph)
                .maximize(self.source, self.target)
		}
	}
}
